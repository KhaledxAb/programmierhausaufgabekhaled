package view;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel {
  private BunteRechteckeController controller = new BunteRechteckeController();

  public Zeichenflaeche(BunteRechteckeController controller) {
    this.controller = controller;

  }

  @Override
  public void paintComponent(Graphics g) {
    Rechteck rechteck0 = controller.getObjekt(0);
    Rechteck rechteck1 = controller.getObjekt(1);
    Rechteck rechteck2 = controller.getObjekt(2);
    Rechteck rechteck3 = controller.getObjekt(3);
    Rechteck rechteck4 = controller.getObjekt(4);
    Rechteck rechteck5 = controller.getObjekt(5);
    Rechteck rechteck6 = controller.getObjekt(6);
    Rechteck rechteck7 = controller.getObjekt(7);
    Rechteck rechteck8 = controller.getObjekt(8);
    Rechteck rechteck9 = controller.getObjekt(9);
    g.setColor(Color.RED);
    g.fillRect(rechteck0.getX(), rechteck0.getY(), rechteck0.getBreite(), rechteck0.getHoehe());
    g.fillRect(rechteck1.getX(), rechteck1.getY(), rechteck1.getBreite(), rechteck1.getHoehe());
    g.fillRect(rechteck2.getX(), rechteck2.getY(), rechteck2.getBreite(), rechteck2.getHoehe());
    g.fillRect(rechteck3.getX(), rechteck3.getY(), rechteck3.getBreite(), rechteck3.getHoehe());
    g.fillRect(rechteck4.getX(), rechteck4.getY(), rechteck4.getBreite(), rechteck4.getHoehe());
    g.fillRect(rechteck5.getX(), rechteck5.getY(), rechteck5.getBreite(), rechteck5.getHoehe());
    g.fillRect(rechteck6.getX(), rechteck6.getY(), rechteck6.getBreite(), rechteck6.getHoehe());
    g.fillRect(rechteck7.getX(), rechteck7.getY(), rechteck7.getBreite(), rechteck7.getHoehe());
    g.fillRect(rechteck8.getX(), rechteck8.getY(), rechteck8.getBreite(), rechteck8.getHoehe());
    g.fillRect(rechteck9.getX(), rechteck9.getY(), rechteck9.getBreite(), rechteck9.getHoehe());
    g.fillRect(rechteck0.getX(), rechteck0.getY(), rechteck0.getBreite(), rechteck0.getHoehe());
    
    
    
    
  
    
  }
}
