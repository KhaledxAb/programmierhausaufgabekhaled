package test;

import controller.BunteRechteckeController;
import model.Punkt;
import model.Rechteck;

public class RechteckTest {
	public static void main(String[] args) {
		Rechteck rechteck0 = new Rechteck();
		Rechteck rechteck1 = new Rechteck();
		Rechteck rechteck2 = new Rechteck();
		Rechteck rechteck3 = new Rechteck();
		Rechteck rechteck4 = new Rechteck();
		// Rechteck 0
		rechteck0.setX(10);
		rechteck0.setY(10);
		rechteck0.setBreite(30);
		rechteck0.setHoehe(40);
		// Rechteck 1
		rechteck1.setX(25);
		rechteck1.setY(25);
		rechteck1.setBreite(100);
		rechteck1.setHoehe(20);
		// Rechteck 2
		rechteck2.setX(260);
		rechteck2.setY(10);
		rechteck2.setBreite(200);
		rechteck2.setHoehe(100);
		// Rechteck 3
		rechteck3.setX(5);
		rechteck3.setY(500);
		rechteck3.setBreite(300);
		rechteck3.setHoehe(25);
		// Rechteck 4
		rechteck4.setX(100);
		rechteck4.setY(100);
		rechteck4.setBreite(100);
		rechteck4.setHoehe(100);
		// Die anderen Rechtecke mit dem vollparametrisierten Konstruktor erstellen
		Rechteck rechteck5 = new Rechteck(200, 200, 200, 200);
		Rechteck rechteck6 = new Rechteck(800, 400, 20, 20);
		Rechteck rechteck7 = new Rechteck(800, 450, 20, 20); 
		Rechteck rechteck8 = new Rechteck(855, 455, 20, 20); //850,400,20,20
		Rechteck rechteck9 = new Rechteck(855, 455, 25, 25); // 855,455,25,25
		System.out.println(rechteck1.equals(rechteck2));

		System.out.println(rechteck0.toString());
		System.out.println(rechteck1.toString());
		BunteRechteckeController controller = new BunteRechteckeController();
		controller.add(rechteck0);
		controller.add(rechteck1);
		controller.add(rechteck2);
		controller.add(rechteck3);
		controller.add(rechteck4);
		controller.add(rechteck5);
		controller.add(rechteck6);
		controller.add(rechteck7);
		controller.add(rechteck8);
		controller.add(rechteck9);
		System.out.println(controller.toString());
		controller.reset();
		System.out.println(controller.toString());
		
		// Programmierhausaufgabe 8 
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
	    System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
	    Rechteck eck11 = new Rechteck();
	    eck11.setX(-10);
	    eck11.setY(-10);
	    eck11.setBreite(-200);
	    eck11.setHoehe(-100);
	    System.out.println(eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
	    //Die Methoden enthaelt einmal mit x und y als Parameter und einmal p als Parameter
	    System.out.println(rechteck0.enthaelt(25, 25));
	    System.out.println(rechteck0.enthaelt(25, 25));
	    Punkt p = new Punkt(25,25);
	    System.out.println(rechteck0.enthaelt(p));
	    System.out.println(rechteck9.enthaelt(p));
	    // Die Methode mit dem Rechteck
	    System.out.println(rechteck9.enthaelt(rechteck8));
	    
	    //Zufall Rechteck
	    Rechteck rechteck10 = Rechteck.generiereZufallsRechteck();
	    System.out.println("X= " + rechteck10.getX() + "Y= " + rechteck10.getY() + "Breite= " + rechteck10.getBreite() + "Hoehe= " 
	    	+ rechteck10.getHoehe());
	    

	}
}
