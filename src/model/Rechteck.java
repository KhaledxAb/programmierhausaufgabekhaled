package model;

import java.util.concurrent.ThreadLocalRandom;

public class Rechteck {

	// private int x;
	// private int y;
	private int breite;
	private int hoehe;
	Punkt p = new Punkt();

	public Rechteck() {
		p.setX(0);
		;
		p.setY(0);
		;
		this.breite = 0;
		this.hoehe = 0;
	}

	public Rechteck(int x, int y, int breite, int hoehe) {
		p.setX(x);
		p.setY(y);
		this.breite = Math.abs(breite);
		this.hoehe = Math.abs(hoehe);
	}

	public int getX() {
		return p.getX();
	}

	public void setX(int x) {
		p.setX(x);
		;
	}

	public int getY() {
		return p.getY();
	}

	public void setY(int y) {
		p.setY(y);
		;
	}

	public int getBreite() {
		if (breite < 0) {
			return breite * -1;
		} else {
			return breite;
		}
	}

	public void setBreite(int breite) {
		if (breite < 0) {
			this.breite = breite * -1;
		} else {
			this.breite = breite;
		}
	}

	public int getHoehe() {
		if (hoehe < 0) {
			return hoehe * -1;
		} else {
			return hoehe;
		}
	}

	public void setHoehe(int hoehe) {
		if (hoehe < 0) {
			this.hoehe = hoehe * -1;
		} else {
			this.hoehe = hoehe;
		}
	}

	public boolean enthaelt(int x, int y) {
		if ((x >= this.p.getX() && x <= (this.p.getX() + this.breite))
				&& (y >= this.p.getY() && y <= (this.p.getY() + this.hoehe))) {
			return true;
		}
		return false;
	}

	public boolean enthaelt(Punkt p) {
		if ((p.getX() >= this.p.getX() && p.getX() <= (this.p.getX() + this.breite))
				&& (p.getY() >= this.p.getY() && p.getY() <= (this.p.getY() + this.hoehe))) {
			return true;
		}
		return false;
	}

	public boolean enthaelt(Rechteck rechteck) {
		if ((rechteck.getX() == this.p.getX() && rechteck.getBreite() <= this.breite)
				&& (rechteck.getY() == this.p.getY() && rechteck.getHoehe() <= this.hoehe)) {
			return true;
		}
		return false;
	}

	public static Rechteck generiereZufallsRechteck() {
		int x = ThreadLocalRandom.current().nextInt(1000, 1200);
		int y = ThreadLocalRandom.current().nextInt(1000, 1200);
		int hoehe = ThreadLocalRandom.current().nextInt(1000, 1200);
		int breite = ThreadLocalRandom.current().nextInt(1000, 1200);
		Rechteck rechteck = new Rechteck(x,y,hoehe,breite);
		return rechteck;

	}

	@Override
	public String toString() {
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite + ", hoehe=" + hoehe + "]";
	}

}
